//
// Created by a.prokopenko on 15.11.2020.
//

#include "matrix.h"
#include <fstream>
#include <iostream>

CMatrix::CMatrix(std::size_t _size)
    : width_(_size), height_(_size), matrix_(_size) {}

CMatrix::CMatrix(std::size_t w_size, std::size_t h_size)
    : width_(w_size), height_(h_size), matrix_(h_size) {}

CMatrix::CMatrix(std::string path) {
	std::ifstream fin(path);
	if (!fin.is_open()) {
		std::cerr << "Can not open file " << path << std::endl;
		throw std::runtime_error("Can not open file " + path);
	}
	fin >> height_ >> width_;
	Resize(width_, height_);
	for (auto &row : matrix_) {
		for (auto &item : row) { fin >> item; }
	}
	fin.close();
}

void CMatrix::Resize(size_t w_size, size_t h_size) {
	matrix_.resize(h_size);
	for (auto &row : matrix_) { row.resize(w_size); }
}

std::ostream &operator<<(std::ostream &os, const CMatrix &matrix) {
	for (const auto &row : matrix.matrix_) {
		for (const auto &item : row) { os << item << " "; }
		os << std::endl;
	}
	return os;
}