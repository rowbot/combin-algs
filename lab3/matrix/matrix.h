//
// Created by a.prokopenko on 15.11.2020.
//

#ifndef COMBIN_LAB3_MATRIX_MATRIX_H_
#define COMBIN_LAB3_MATRIX_MATRIX_H_

#include <cstddef>
#include <string>
#include <tuple>
#include <vector>

class CMatrix {
 public:
	CMatrix() = default;

	explicit CMatrix(std::string path);

	explicit CMatrix(std::size_t _size);

	CMatrix(std::size_t w_size, std::size_t h_size);

	CMatrix(const CMatrix &matrix) = default;

	CMatrix(CMatrix &&that) = default;

	CMatrix &operator=(const CMatrix &other) = default;

	CMatrix &operator=(CMatrix &&that) = default;

	std::vector<int> &operator[](int i) { return matrix_[i]; }

	void Resize(size_t w_size, size_t h_size);

	[[nodiscard]] auto GetSize() const {
		return std::make_tuple(height_, width_);
	}

	[[nodiscard]] auto Rows() const { return height_; }
	[[nodiscard]] auto Cols() const { return width_; }

	friend std::ostream &operator<<(std::ostream &os, const CMatrix &dt);

	~CMatrix() = default;

 private:
	std::size_t width_ = 0;
	std::size_t height_ = 0;
	std::vector<std::vector<int>> matrix_{};
};

#endif//COMBIN_LAB3_MATRIX_MATRIX_H_
