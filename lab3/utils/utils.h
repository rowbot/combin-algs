//
// Created by a.prokopenko on 15.11.2020.
//

#ifndef COMBIN_LAB3_UTILS_UTILS_H_
#define COMBIN_LAB3_UTILS_UTILS_H_

#include <vector>

namespace utils {

bool hasElement(const std::vector<int> &v, int elem);

void print(const std::vector<int> &v);

int getIndex(std::vector<int> &v, int elem);


}// namespace utils

#endif//COMBIN_LAB3_UTILS_UTILS_H_
