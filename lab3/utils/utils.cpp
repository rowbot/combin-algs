//
// Created by a.prokopenko on 15.11.2020.
//
#include <iostream>
#include <vector>

#include "utils/utils.h"

namespace utils {

bool hasElement(const std::vector<int> &v, int elem) {
	return std::find(v.begin(), v.end(), elem) != v.end();
}

void print(const std::vector<int> &v) {
	for (int i : v) { std::cout << i << ' '; }
	std::cout << std::endl;
}

int getIndex(std::vector<int> &v, int elem) {
	auto it = find(v.begin(), v.end(), elem);
	if (it != v.end()) { return it - v.begin(); }
	return -1;
}

}// namespace utils