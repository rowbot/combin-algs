//
// Created by a.prokopenko on 15.11.2020.
//

#include <cassert>
#include <iostream>
#include <map>

#include "algorithm/hall.h"
#include "utils/utils.h"

HallsAlg::HallsAlg(CMatrix &matrix)
    : matrix_(matrix.Rows()), sets_(matrix.Rows()) {
  for (int i = 0; i < matrix.Rows(); ++i) {
    for (int j = 0; j < matrix.Cols(); ++j) {
      if (matrix[i][j]) {
        sets_[i].push_back(j + 1);
      }
    }
  }
}

void HallsAlg::PrintSets() const {
  for (int i = 0; i < sets_.size(); ++i) {
    std::cout << 'S' << i << ": ";
    for (const auto &item : sets_[i]) {
      std::cout << item << ' ';
    }
    std::cout << std::endl;
  }
}

std::vector<int> HallsAlg::InitT0_() {
  std::vector<int> initial_T{};
  for (auto &i : sets_) {
    bool has_elem = false;
    for (const auto &j : i) {
      if (!utils::hasElement(initial_T, j)) {
        initial_T.push_back(j);
        has_elem = true;
        break;
      }
    }
    if (!has_elem) {
      return std::move(initial_T);
    }
  }
  return std::move(initial_T);
}

void HallsAlg::Solve() {
  auto initial_T = InitT0_();
  std::cout << "T[0]: ";
  utils::print(initial_T);
  int i = 1;
  while (initial_T.size() != sets_.size()) {
    std::cout << "_______(" << i << ")_______" << std::endl;
    initial_T = FindNext_(initial_T);
    std::cout << "T: ";
    utils::print(initial_T);
    ++i;
  }
}

std::vector<int> HallsAlg::FindNext_(std::vector<int> &current_T) {
  int next_set = current_T.size();
  auto L = sets_[next_set]; // L_0
  std::cout << "L[0]: ";
  utils::print(L);

  int i = 0;
  int j = utils::getIndex(current_T, L[0]);
  std::map<int, std::vector<int>> added_sets;
  while (j != -1) { // scroll and additions into L
    if (added_sets.find(j) != added_sets.end()) {
      std::cerr << "RED ALERT: map key is overrode" << std::endl;
    }
    added_sets[j] = sets_[j];
    MergeSets_(L, sets_[j]);
    std::cout << "L[" << i + 1 << "]: ";
    utils::print(L);
    i++;
    j = utils::getIndex(current_T, L[i]);
  }

  int new_reprs = L[i];
  int change_index = -1;
  // changing representative in R position of current transversal
  std::vector<int> T_new(current_T);
  while (!utils::hasElement(sets_[next_set], new_reprs)) {
    change_index = GetIndexFromSet_(added_sets, new_reprs, change_index);
    assert(change_index != -1);
    std::swap(T_new[change_index], new_reprs);
  }

  T_new.push_back(new_reprs);
  return std::move(T_new);
}

void HallsAlg::MergeSets_(std::vector<int> &s1, std::vector<int> const &s2) {
  for (const auto &item : s2) {
    if (!utils::hasElement(s1, item)) {
      s1.push_back(item);
    }
  }
}

int HallsAlg::GetIndexFromSet_(std::map<int, std::vector<int>> &added_sets,
                               int item, int idx) {
  for (const auto &[key, value] : added_sets) {
    if (key > idx && utils::hasElement(value, item)) {
      return key;
    }
  }
  return -1;
}