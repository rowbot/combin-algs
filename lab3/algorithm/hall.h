//
// Created by a.prokopenko on 15.11.2020.
//

#ifndef COMBIN_LAB3_ALGORITHM_HALL_H_
#define COMBIN_LAB3_ALGORITHM_HALL_H_

#include <map>

#include "matrix/matrix.h"

typedef std::vector<std::vector<int>> array2d_t;

class HallsAlg {
 public:
	HallsAlg() = delete;
	explicit HallsAlg(CMatrix &matrix);

	void PrintSets() const;

	void Solve();

	~HallsAlg() = default;

 private:
	std::vector<int> InitT0_();
	std::vector<int> FindNext_(std::vector<int> &current_T);
	void MergeSets_(std::vector<int> &s1, std::vector<int> const &s2);
	int GetIndexFromSet_(std::map<int, std::vector<int>> &added_sets, int item,
	                     int idx = -1);

	CMatrix matrix_;
	array2d_t sets_{};
};

#endif//COMBIN_LAB3_ALGORITHM_HALL_H_
