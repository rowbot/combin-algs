#include <iostream>

#include "algorithm/hall.h"
#include "matrix/matrix.h"

int main(int argc, char **argv) {
	std::string path = "matrix.txt";
	if (argc > 2) {
		std::string key(argv[1]);
		if (key == "-p") { path = argv[2]; }
	}
	CMatrix matrix(path);
	std::cout << matrix << std::endl;
	HallsAlg alg(matrix);
	std::cout << "Sets:" << std::endl;
	alg.PrintSets();
	alg.Solve();
	return 0;
}
