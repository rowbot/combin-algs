cmake_minimum_required(VERSION 3.17)
project(combin_lab4)

set(CMAKE_CXX_STANDARD 20)

include_directories(${CMAKE_CURRENT_SOURCE_DIR})
set(DIR ${CMAKE_CURRENT_SOURCE_DIR})

add_executable(${PROJECT_NAME}-sum sum_partitions_of_24.cpp)

add_executable(${PROJECT_NAME}-all all_partitions_of_9.cpp)