//
// Created by rowbotman on 06/12/2020.
//
#include <functional>
#include <iostream>
#include <map>
#include <vector>

typedef std::vector<int> vector_t;

int findMaxAdd(vector_t &adds) {
  for (int i = static_cast<int>(adds.size()) - 1; i >= 0; --i) {
    if (adds[i] + 2 <= adds.back()) {
      return i;
    }
  }
  return -1;
}

void hindenburgAlg(int n) {
  vector_t current_adds{};
  current_adds.push_back(n); // first step - partition which is the n
  bool stop = false;
  while (!stop) {
    std::for_each(current_adds.begin(), current_adds.end(),
                  [](int &item) { std::cout << item << " "; });
    std::cout << std::endl;
    int maxIdx = findMaxAdd(current_adds);
    if (maxIdx == -1) {
      // next series of lexicographic partitions (m+1 terms)
      if (n != current_adds.size() + 1) {
        int m = static_cast<int>(current_adds.size());
        current_adds.resize(m + 1, 1);
        std::fill(current_adds.begin(), current_adds.end(), 1);
        current_adds.back() = n - m; //.push_back(n - m);
      } else {
        stop = true;
      }
      continue;
    }
    ++current_adds[maxIdx];
    std::for_each(
        current_adds.begin() + maxIdx, current_adds.end() - 1,
        [&current_adds, maxIdx](int &item) { item = current_adds[maxIdx]; });
    int sum = 0;
    std::for_each(current_adds.begin(), current_adds.end() - 1,
                  [&sum](int &item) { sum += item; });
    current_adds.back() = n - sum;
  }
  for (int _ = 0; _ < n; ++_) {
    std::cout << 1 << ' ';
  }
}

/**
 * Ehrlich's looplees algorithm for the set of the partitions of [n]
 * I could not find an implementation of it and had to write it by myself
 * So, I warn you for future usage that it is might not work very well.
 * Issues are welcome
 * @param n
 */
void ehrlichAlg(int n) {
  vector_t multi_sum(n + 1, 0);
  multi_sum[1] = n;
  bool stop = false;
  while (!stop) {
    int p = n;
    vector_t vec_p{};
    std::for_each(multi_sum.rbegin(), multi_sum.rend(),
                  [&p, &vec_p](int &item) {
                    if (item && p) {
                      vec_p.push_back(p);
                      for (int i = 0; i < item; ++i) {
                        std::cout << p << " ";
                      }
                    }
                    --p;
                  });
    int m = vec_p.back();
    std::cout << std::endl;
    if (multi_sum[m] > 1) {
      multi_sum[m] -= 2;
      multi_sum[m + 1] += 1;
      if (m != 1) {
        multi_sum[1] = 2 * m - (m + 1);
      }
    } else if (multi_sum[m] == 1) {
      int new_term = vec_p[vec_p.size() - 2] + 1;
      multi_sum[new_term] += 1;
      multi_sum[m] -= 1;
      int prev_m = vec_p[vec_p.size() - 2];
      multi_sum[prev_m] -= 1;
      multi_sum[1] = prev_m * multi_sum[prev_m];
      multi_sum[prev_m] = 0;
      if (m != 1) {
        multi_sum[1] = m + prev_m - new_term;
      }
      if (multi_sum[multi_sum.size() - 1]) {
        stop = true;
      }
    }
  }
  std::cout << n << std::endl;
}

int main(int argc, char **argv) {
  const int kN = 9;
  std::function<void(int)> algorithm = hindenburgAlg;
  if (argc > 1) {
    std::string key(argv[1]);
    if (key == "-e") {
      algorithm = ehrlichAlg;
    }
  }
  algorithm(kN);
  std::cout << std::endl;
  return 0;
}