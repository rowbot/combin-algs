//
// Created by starman on 06/12/2020.
//

#include <iostream>

int calcP(int n, int m = 1){
  if (n < 0 || m < 0) {
    return 1;
  }
  if (n == 1 || m == 1) {
    return 1;
  } else if (m > n) {
    return calcP(n, n);
  } else if (m == n) {
    return calcP(n, n - 1) + 1;
  }
  return calcP(n, m - 1) + calcP(n - m, m);
}

int main(int argc, char **argv) {
  const int kN = 24;
  std::cout << calcP(kN, kN) << std::endl;
  return 0;
}