#include <iostream>
#include <vector>

#include "src/matrix.h"
#include "src/gauss.h"

#define FREE_VARS 3
#define VARS_NUM 5

void solve_eqaution_and_print(std::vector<int>& basis) {
	// equation matrix initialization
    Matrix left(FREE_VARS, VARS_NUM);
    double initial_eqs[FREE_VARS][VARS_NUM] = {
        {1, 1, 1, 0, 0},
        {-1, 1, 0, 1, 0},
        {2, 1, 0, 0, 1},
    };
    left.ArrayToMatrix(*initial_eqs, FREE_VARS, VARS_NUM);

	// right side initialization
    Matrix right(FREE_VARS, 1);
    double initial_right[FREE_VARS][1] = {
        {4},
        {2},
        {6},
    };
    right.ArrayToMatrix(*initial_right, FREE_VARS, 1);

    int non_basis[VARS_NUM - FREE_VARS]{};
    // basic vars will be set as true
    std::vector<bool> exists_in_basis(VARS_NUM, false);
    for (int i = 0; i < FREE_VARS; i++) {
        exists_in_basis[basis[i] - 1] = true;
    }
    for (int j = 0, i = 0; i < VARS_NUM; i++) {
        if (!exists_in_basis[i]) {
            non_basis[j] = i;
            ++j;
        }
    }
    // remove not neccesary columnsd
    left.ReduceColumns(non_basis, VARS_NUM - FREE_VARS);
    gauss(left, right);
    for (int i = 0; i < FREE_VARS; i++) {
        std::cout << 'x' << basis[i] << " = " << right[i][0] << ", ";
    }
    for (int i = 0; i < VARS_NUM - FREE_VARS; i++) {
        std::cout << 'x' << non_basis[i] + 1 << " = 0";
        if (i != VARS_NUM - FREE_VARS - 1) {
            std::cout << ", ";
        }
    }
    std::cout << std::endl;
}

int main(int argc, char** argv) {
    // intial combs
    std::vector<int> c_max = {3, 4, 5};
    std::vector<int> c_vec = {1, 2, 3};
    solve_eqaution_and_print(c_vec);
    while (c_vec[0] != c_max[0]) {
        int i_change = 0;
        for (int i = FREE_VARS - 1; i >= 0; --i) {
            if (c_vec[i] < c_max[i]) {
                i_change = i;
				break;
            }
        }
        // increasing next elements
        ++c_vec[i_change];
        for (int j = i_change + 1; j < FREE_VARS; ++j) {
            c_vec[j] = c_vec[i_change] + j - i_change;
        }
        solve_eqaution_and_print(c_vec);
    }
    return 0;
}
