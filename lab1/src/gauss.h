#ifndef SRC_GAUSS_H_
#define SRC_GAUSS_H_

#include "src/matrix.h"

void makeDiagOneGauss(Matrix &left, Matrix &right, int i_from);

void subRowsGauss(Matrix &left, Matrix &right, int i_from, int i_to, double coeff);

void forwardGauss(Matrix &left, Matrix &right);

void backwardGauss(Matrix &left, Matrix &right);

void gauss(Matrix &left, Matrix &right);

#endif  // SRC_GAUSS_H_
