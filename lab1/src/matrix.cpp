#include <iomanip>
#include <iostream>

#include "src/matrix.h"

Matrix::Matrix(const int rows, const int cols) {
    size1_ = rows;
    size2_ = cols;
    matrix_ = new double*[rows];
    for (int i = 0; i < rows; i++) {
        matrix_[i] = new double[cols];
    }
}

const int Matrix::GetSize1() const {
    return size1_;
}

const int Matrix::GetSize2() const {
    return size2_;
}

const double Matrix::get(int i, int j) const {
    if (!check_i_j(i, j)) {
        return 0;
    }
    return matrix_[i][j];
}

void Matrix::set(int i, int j, double val) {
    if (!check_i_j(i, j)) {
        return;
    }
    matrix_[i][j] = val;
}

const bool Matrix::check_i_j(int i, int j) const {
    if (i >= size1_) {
        std::cout << "too high row:" << i << std::endl;
        return false;
    } else if (j >= size2_) {
        std::cout << "too high col:" << j << std::endl;
        return false;
    } else if (i < 0) {
        std::cout << "too low row:" << i << std::endl;
        return false;
    } else if (j < 0) {
        std::cout << "too low col:" << j << std::endl;
        return false;
    }
    return true;
}

void Matrix::MakeDiagOneGauss(int i_from, Matrix& fr) {
    double coeff = matrix_[i_from][i_from];
    for (int j = 0; j < size2_; j++) {
        matrix_[i_from][j] = matrix_[i_from][j] / coeff;
    }
    fr.set(i_from, 0, (fr.get(i_from, 0) / coeff));
}

void Matrix::SubRowsGauss(int i_from, int i_to, double coeff, Matrix& fr) {
    for (int j = 0; j < size2_; j++) {
        matrix_[i_to][j] -= matrix_[i_from][j] * coeff;
    }
    fr.set(i_to, 0, fr.get(i_to, 0) - (fr.get(i_from, 0) * coeff));
}

void Matrix::_ForwardGauss(Matrix& fr) {
    for (int i = 0; i < GetSize1() - 1; i++) {
        for (int k = i + 1; k < GetSize1(); k++) {
            MakeDiagOneGauss(i, fr);
            double coeff = matrix_[k][i];
            SubRowsGauss(i, k, coeff, fr);
        }
    }
    MakeDiagOneGauss(GetSize1() - 1, fr);
}

void Matrix::_BackwardGauss(Matrix& fr) {
    for (int i = GetSize1() - 1; i > 0; i--) {
        for (int k = i - 1; k >= 0; k--) {
            double coeff = matrix_[k][i];
            SubRowsGauss(i, k, coeff, fr);
        }
    }
}

void Matrix::Gauss(Matrix& fr) {
    _ForwardGauss(fr);
    _BackwardGauss(fr);
}

Matrix::~Matrix() {
    for (int i = 0; i < size1_; i++) {
        delete[] matrix_[i];
    }
    delete[] matrix_;
}

void Matrix::ReduceColumns(int j_dels[], const int n) {
    double** newmatrix_ = new double*[size1_];
    for (int i = 0; i < size1_; i++) {
        newmatrix_[i] = new double[size2_ - n];
    }
    bool del[size2_];
    for (int i = 0; i < size2_; i++) {
        del[i] = false;
    }
    for (int j = 0; j < n; j++) {
        del[j_dels[j]] = true;
    }
    for (int i = 0; i < size1_; i++) {
        int c = 0;
        for (int j = 0; j < size2_; j++) {
            if (!del[j]) {
                newmatrix_[i][c] = matrix_[i][j];
                c++;
            }
        }
    }
    for (int i = 0; i < size1_; i++) {
        delete[] matrix_[i];
    }
    delete[] matrix_;
    matrix_ = newmatrix_;
    size2_ = size2_ - n;
}
