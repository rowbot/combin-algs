#include <iostream>

#include "src/gauss.h"

void makeDiagOneGauss(Matrix &left, Matrix &right, int from) {
    double coeff = left[from][from];
    for (int j = 0; j < left.GetSize2(); j++) {
        left[from][j] = left[from][j] / coeff;
    }
    right.set(from, 0, right[from][0] / coeff);
}

void subRowsGauss(Matrix &left, Matrix &right, int from, int to, double coeff) {
    for (int j = 0; j < left.GetSize2(); j++) {
        left[to][j] -= left[from][j] * coeff;
    }
    right.set(to, 0, right[to][0] - right[from][0] * coeff);
}

void forwardGauss(Matrix &left, Matrix &right) {
    for (int i = 0; i < left.GetSize1() - 1; i++) {
        for (int k = i + 1; k < left.GetSize1(); k++) {
            makeDiagOneGauss(left, right, i);
            double coeff = left[k][i];
            subRowsGauss(left, right, i, k, coeff);
        }
    }
    makeDiagOneGauss(left, right, left.GetSize1() - 1);
}

void backwardGauss(Matrix &left, Matrix &right) {
    for (int i = left.GetSize1() - 1; i > 0; --i) {
        for (int k = i - 1; k >= 0; --k) {
            double coeff = left[k][i];
            subRowsGauss(left, right, i, k, coeff);
        }
    }
}

void gauss(Matrix &left, Matrix &right) {
    forwardGauss(left, right);
    backwardGauss(left, right);
}