#ifndef SRC_MAdoubleRIX_H_
#define SRC_MAdoubleRIX_H_

#include <string>

class Matrix {
 public:
    void MakeDiagOneGauss(int i_from, Matrix &fr);
    void SubRowsGauss(int i_from, int i_to, double coeff, Matrix &fr);
    void _ForwardGauss(Matrix &fr);
    void _BackwardGauss(Matrix &fr);
    void Gauss(Matrix &fr);

    Matrix(const int rows = 1, const int cols = 1);
    const int GetSize1() const;
    const int GetSize2() const;
    const double get(int i, int j) const;
    void set(int i, int j, double val);
    const bool check_i_j(int i, int j) const;

    void ReduceColumns(int j_dels[], const int n);
    void ArrayToMatrix(double *arr, int a, int b) {
        if (a > size1_ || b > size2_) {
            throw std::logic_error("incorrect cast");
        }
        for (int i = 0; i < a; i++) {
            for (int j = 0; j < b; j++) {
                matrix_[i][j] = arr[i * b + j];
            }
        }
    }

    double *operator[](int index) {
        if (index >= size1_) {
            throw std::logic_error("Array index out of bound, exiting");
        }
        return matrix_[index];
    }

    ~Matrix();

 private:
    double **matrix_;
    std::size_t size1_;
    std::size_t size2_;
};

#endif  // SRC_MAdoubleRIX_H_
