//
// Created by starman on 26/10/2020.
//

#ifndef COMBINLABS__LENGTH_H_
#define COMBINLABS__LENGTH_H_

#include <vector>

bool isLenOK(std::vector<int>& vec, size_t needed);

#endif  // COMBINLABS__LENGTH_H_
