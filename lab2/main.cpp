#include <iostream>
#include <vector>
#include "length.h"

// Variant 14
// Используя алгоритим вращения перестановок, найти все варианты расположить
// 5 ладей на шахматной доске, размером 5х5, когда они не угрожают друг другу,
// отмечая варианты, где они занимают только белые поля.

class ChessBoard {
 public:
    explicit ChessBoard(size_t _size = 8)
        : size_(_size), step_(0), max_step_(_size), current_(_size) {
        int s_one = 1;
        for (int i = 0; i < size_; ++i) {
            current_[i] = s_one + i;
        }
        history_.push_back(current_);
    }

    ChessBoard() = delete;

    void Replace() {
        int n = 0;
        while (max_step_ > 1) {
            if (n && FoundMatch_()) {
                AddRecord_(current_);
            }
            PrintCurrent(n);
            ++n;
            MakeMove_();
        }
    }

    void PrintCurrent(int n_iter) {
        int i = 1;
        bool ok = true;
        for (const auto &item : current_) {
            if (i % 2 == 0 && item % 2 == 0 || i % 2 != 0 && item % 2 != 0) {
            } else {
                ok = false;
                break;
            }
            i++;
        }
        if (ok) {
            ++count_;
            Print_(current_, n_iter);
        }
    }

 private:
    void Print_(std::vector<int> &vec, int num = 0) {
        //		std::cout << num << ' ';
        std::cout << "(" << count_ << ") : |";
        if (!isLenOK(vec, count_)) {
            return;
        }
        for (const auto &item : current_) {
            std::cout << item << '|';
        }
        std::cout << std::endl;
    }
    void Rotate_() {
        std::vector<int> saved(step_);  // 5, 4
        for (int i = 0; i < step_; ++i) {
            saved[i] = current_.back();
            current_.pop_back();
        }
        auto last = current_.back();  // 1,2,3 | 4,5
        if (!current_.empty()) {
            current_.pop_back();
        }
        current_.insert(current_.cbegin(), last);
        while (!saved.empty()) {
            current_.push_back(saved.back());
            saved.pop_back();
        }
    }

    void MakeMove_() {
        auto item = current_.back();
        current_.pop_back();
        current_.insert(current_.cbegin(), item);
    }

    bool FoundMatch_() {
        bool match = false;
        step_ = 1;
        size_t j = 0;
        for (const auto &record : history_) {
            if (record[size_ - step_] == current_[size_ - step_]) {
                Rotate_();
                step_++;
                match = true;
            }
        }
        if (size_ - step_ < max_step_) {
            max_step_ = size_ - step_;
        }
        if (j + 1 == max_step_) {
            --max_step_;
        }
        return match;
    }

    void AddRecord_(std::vector<int> &new_record) {
        if (history_.size() < step_) {
            history_.insert(history_.cbegin(), new_record);
        } else {
            history_[step_ - 2] = new_record;
        }
    }

    size_t size_ = 0;
    size_t step_ = 0;
    size_t max_step_ = 0;
    size_t count_ = 0;
    std::vector<int> current_{};
    std::vector<std::vector<int>> history_{};
};

int main() {
    ChessBoard board(5);
    board.Replace();
    return 0;
}
